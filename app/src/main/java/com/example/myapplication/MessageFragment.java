package com.example.myapplication;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import java.util.ArrayList;
import java.util.List;

public class MessageFragment extends Fragment {



    public ArrayList<String> numbers = new ArrayList<>();
    public ArrayList<String> emails = new ArrayList<>();
    public static SparseBooleanArray checkedItems;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_message, container, false);

       // Get all of the items that have been clicked - either on or off
        if(ContactsFragment.conList!=null){
         checkedItems = ContactsFragment.conList.getCheckedItemPositions();

        if(checkedItems.size()>0) {
            for (int i = 0; i < checkedItems.size(); i++) {
                // And this tells us the item status at the above position
                final boolean isChecked = checkedItems.valueAt(i);
                if (isChecked) {
                    // This tells us the item position we are looking at
                    final int position = checkedItems.keyAt(i);
                    // Put the value of the phone number and the email in our list
                    numbers.add(ContactsFragment.numbersList.get(position));
                    numbers.add(";");
                    emails.add(ContactsFragment.emailList.get(position));

                }
            }
            if(numbers.size()>0){
                numbers.remove(numbers.size() - 1);
            }
            else{
                Toast.makeText(getContext(), "No contacts selected!", Toast.LENGTH_SHORT).show();
            }

        }
        }
        else {
            Toast.makeText(getContext(), "No contacts selected!", Toast.LENGTH_SHORT).show();
        }
        //Listen for MMS button
        Button mmsButton = (Button)root.findViewById(R.id.mms);
        mmsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(numbers.size()<=0){
                    Toast.makeText(getContext(), "No contacts selected!", Toast.LENGTH_SHORT).show();
                }
                else {
                    sendSMS();
                }
            }
        });
        //Listen to Email button
        Button mailButton = (Button)root.findViewById(R.id.email);
        mailButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(numbers.size()<=0){
                    Toast.makeText(getContext(), "No contacts selected!", Toast.LENGTH_SHORT).show();
                }
                else {
                    sendEmail();
                }
            }
        });
        return root;
    }

    public void sendSMS(){
        try {

            startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", numbers.toString(), null)));
        }

        catch(Exception e){
            Toast.makeText(getContext(), "SMS Failed to Send, Please try again", Toast.LENGTH_SHORT).show();
        }
    }

    public void sendEmail(){
        String temp="";
        for(int i=0; i<emails.size();i++) {
            temp=temp+emails.get(i)+",";
        }
        Toast.makeText(getContext(), temp, Toast.LENGTH_SHORT).show();
            Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto",temp, null));

        try {

            startActivity(Intent.createChooser(emailIntent, "Choose an Email client :"));

        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(getContext(), "There is no email client installed.", Toast.LENGTH_SHORT).show();
        }
    }

}
