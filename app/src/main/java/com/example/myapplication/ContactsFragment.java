package com.example.myapplication;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import java.util.ArrayList;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class ContactsFragment extends Fragment {

    public static ListView conList;
    public static View root;
    public static ArrayList<String> numbersList = new ArrayList<>();
    public static ArrayList<String> emailList = new ArrayList<>();
    private static final int PERMISSIONS_REQUEST_READ_CONTACTS = 100;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        //Saving the ListView when changing fragments
        if(root==null){
         root = inflater.inflate(R.layout.fragment_contacts, container, false);
        }
        else {
            return root;
        }
        conList = root.findViewById(R.id.contactList);

        showContacts();

        return root;
    }


    private void showContacts() {
        // Check the SDK version and whether the permission is already granted or not.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ContextCompat.checkSelfPermission(
                this.getActivity(),Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, PERMISSIONS_REQUEST_READ_CONTACTS);
            //After this point you wait for callback in onRequestPermissionsResult(int, String[], int[]) overriden method
        } else {
            // Android version is lesser than 6.0 or the permission is already granted.
            ArrayList<String> contacts = new ArrayList<String>();
            //Store contacts from phone and display them as a list
            contacts=loadContacts();
            ArrayAdapter<String>arrayAdapter = new ArrayAdapter<String>(getContext(),
                    android.R.layout.simple_list_item_multiple_choice, contacts);
            conList.setAdapter(arrayAdapter);
            conList.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_READ_CONTACTS) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission is granted
                showContacts();
            } else {
                Toast.makeText(getContext(), "Until you grant the permission, we canot display the names", Toast.LENGTH_SHORT).show();
            }
        }
    }


  public ArrayList<String> loadContacts() {
      ArrayList<String> nameList = new ArrayList<>();
      Cursor contact = getActivity().getContentResolver().query(ContactsContract.Contacts.CONTENT_URI,
              null, null, null, null);

      //int numberFieldColumnIndex = contact.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
      int nameFieldColumnIndex = contact.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);
      while (contact.moveToNext()) {
          //Find name and id
          String id = contact.getString(contact.getColumnIndex(ContactsContract.Contacts._ID));
          String name = contact.getString(nameFieldColumnIndex);
          nameList.add(name);
          if (contact.getInt(contact.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)) > 0) {
              Cursor pCur = getActivity().getContentResolver().query(
                      ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                      null,
                      ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                      new String[]{id}, null);
              //Find phone number
              while (pCur.moveToNext()) {
                  String phoneNo = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                  numbersList.add(phoneNo);

              }
              pCur.close();
              // Find Email Addresses
              Cursor emails = getActivity().getContentResolver().query(
                      ContactsContract.CommonDataKinds.Email.CONTENT_URI,null,
                      ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = " + id,null,
                      null);
              while (emails.moveToNext())
              {
                   String emailAddress = emails.getString(emails.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                   emailList.add(emailAddress);
              }
              emails.close();
          }
      }
      contact.close();
      return nameList;
  }

}